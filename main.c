#define TZ_CALIFORNIA "TZ=PST8PDT"
#define STARTING_YEAR 1900

main()
{
     time_t now;
     struct tm *sp;
     if (putenv(TZ_CALIFORNIA) != 0)
     {
        fprintf(stderr, "putenv(): Insufficient memory was available.\n");
        exit(errno);
     }
     (void) time(&now);

     sp = localtime(&now);
     printf("%d/%d/%02d %d:%02d %s\n",
         sp->tm_mon + 1, sp->tm_mday,
         STARTING_YEAR + sp->tm_year, sp->tm_hour,
         sp->tm_min, tzname[sp->tm_isdst]);
     exit(0);
}
